package com.desafioandroid.presentation.viewmodel

import com.desafioandroid.data.model.PullRequest
import com.desafioandroid.domain.interactors.pullrequest.GetPullRequestsInteractor
import com.desafioandroid.presentation.state.BrowsePullRequestsState
import com.desafioandroid.util.TestUtils
import com.desafioandroid.util.TestUtils.createPullRequests
import com.desafioandroid.util.TestUtils.randomInt
import com.desafioandroid.util.TestUtils.randomString
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

class BrowsePullRequestsViewModelTest {
  private val interactor = mockk<GetPullRequestsInteractor>()
  private val viewModel = BrowsePullRequestsViewModel(interactor)

  @Before
  fun setup() {
    viewModel.nomeCriador = randomString()
    viewModel.nomeRepositorio = randomString()
  }

  @Test fun `fetch pull requests executes interactor`() {
    stubExecute(Single.just(TestUtils.createPullRequests()))

    viewModel.fetchPullRequests(randomInt())

    verify(exactly = 1) { interactor.execute(any(), any(), any()) }
  }

  @Test fun `fetch pull requests returns data emit data view state`() {
    val data = createPullRequests()
    stubExecute(Single.just(data))

    val testObserver = viewModel.getState()
        .test()

    viewModel.fetchPullRequests(randomInt())

    testObserver.assertValue(BrowsePullRequestsState.Data(data))
    testObserver.assertNoErrors()
  }

  @Test fun `fetch projects returns error emit error view state`() {
    val error = RuntimeException()
    every { interactor.execute(any(), any(), any()) } returns Single.error(error)

    val testObserver = viewModel.getState()
        .test()

    viewModel.fetchPullRequests(randomInt())

    testObserver.assertValue(BrowsePullRequestsState.Error(error))
  }

  private fun stubExecute(single: Single<List<PullRequest>>) {
    every { interactor.execute(any(), any(), any()) } returns single
  }
}