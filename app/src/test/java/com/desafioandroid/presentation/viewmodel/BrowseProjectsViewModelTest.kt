package com.desafioandroid.presentation.viewmodel

import com.desafioandroid.data.model.Project
import com.desafioandroid.domain.interactors.project.GetProjectsInteractor
import com.desafioandroid.presentation.state.BrowseProjectsState
import com.desafioandroid.util.TestUtils.createProjects
import com.desafioandroid.util.TestUtils.randomInt
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Single
import org.junit.Test

class BrowseProjectsViewModelTest {
  private val interactor = mockk<GetProjectsInteractor>()
  private val viewModel = BrowseProjectsViewModel(interactor)

  @Test fun `fetch projects executes interactor`() {
    stubExecute(Single.just(createProjects()))

    viewModel.fetchProjects(randomInt())

    verify(exactly = 1) { interactor.execute(any()) }
  }

  @Test fun `fetch projects returns data emit data view state`() {
    val data = createProjects()
    stubExecute(Single.just(data))

    val testObserver = viewModel.getState()
        .test()

    viewModel.fetchProjects(randomInt())

    testObserver.assertValue(BrowseProjectsState.Data(data))
    testObserver.assertNoErrors()
  }

  @Test fun `fetch projects returns error emit error view state`() {
    val error = RuntimeException()
    every { interactor.execute(any()) } returns Single.error(error)

    val testObserver = viewModel.getState()
        .test()

    viewModel.fetchProjects(randomInt())

    testObserver.assertValue(BrowseProjectsState.Error(error))
  }

  private fun stubExecute(single: Single<List<Project>>) {
    every { interactor.execute(any()) } returns single
  }
}