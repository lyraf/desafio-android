package com.desafioandroid.data.repository

import com.desafioandroid.data.model.PullRequest
import com.desafioandroid.data.store.RemoteStore
import com.desafioandroid.util.TestUtils.createPullRequests
import com.desafioandroid.util.TestUtils.randomInt
import com.desafioandroid.util.TestUtils.randomString
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Single
import org.junit.Test

class PullRequestRepositoryImplTest {
  private val remote = mockk<RemoteStore>()
  private val repository = PullRequestRepositoryImpl(remote)

  @Test fun `get pull requests completes`() {
    stubGetPullRequests(Single.just(createPullRequests()))

    val testObserver = repository.getPullRequests(randomString(), randomString(), randomInt())
        .test()

    testObserver.assertComplete()
  }

  @Test fun `get pull requests returns data`() {
    val data = createPullRequests()
    stubGetPullRequests(Single.just(data))

    val testObserver = repository.getPullRequests(randomString(), randomString(), randomInt())
        .test()

    testObserver.assertValue(data)
  }

  private fun stubGetPullRequests(single: Single<List<PullRequest>>) {
    every { remote.getPullRequests(any(), any(), any()) } returns single
  }
}