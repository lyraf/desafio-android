package com.desafioandroid.data.repository

import com.desafioandroid.data.model.Project
import com.desafioandroid.data.store.RemoteStore
import com.desafioandroid.util.TestUtils.createProjects
import com.desafioandroid.util.TestUtils.randomInt
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Single
import org.junit.Test

class ProjectRepositoryImplTest {
  private val remote = mockk<RemoteStore>()
  private val repository = ProjectRepositoryImpl(remote)

  @Test fun `get projects completes`() {
    stubGetProjects(Single.just(createProjects()))

    val testObserver = repository.getProjects(randomInt())
        .test()

    testObserver.assertComplete()
  }

  @Test fun `get projects returns data`() {
    val data = createProjects()
    stubGetProjects(Single.just(data))

    val testObserver = repository.getProjects(randomInt())
        .test()

    testObserver.assertValue(data)
  }

  private fun stubGetProjects(single: Single<List<Project>>) {
    every { remote.getProjects(any()) } returns single
  }
}