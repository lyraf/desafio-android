package com.desafioandroid.util

import com.desafioandroid.remote.model.ApiResponse
import com.desafioandroid.data.model.PullRequest
import com.desafioandroid.data.model.Project
import com.desafioandroid.data.model.User
import java.util.UUID
import java.util.concurrent.ThreadLocalRandom

object TestUtils {
  fun randomString(): String {
    return UUID.randomUUID()
        .toString()
  }

  fun randomInt(): Int {
    return ThreadLocalRandom.current()
        .nextInt(0, 1000 + 1)
  }

  fun createProjects(): List<Project> {
    return (0..4).map { createProject() }
  }

  fun createPullRequests(): List<PullRequest> {
    return (0..5).map { createPullRequest() }
  }

  fun createApiReponseProjects(projects: List<Project>): ApiResponse<Project> {
    return ApiResponse(projects)
  }

  private fun createUser(): User {
    return User(randomString(), randomString())
  }

  private fun createProject(): Project {
    return Project(
        randomString(),
        randomString(),
        randomInt(),
        randomInt(),
        createUser()
    )
  }

  private fun createPullRequest(): PullRequest {
    return PullRequest(
        randomString(),
        randomString(),
        randomString(),
        randomString(),
        randomString(),
        createUser()
    )
  }
}