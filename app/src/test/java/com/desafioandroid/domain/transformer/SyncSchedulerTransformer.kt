package com.desafioandroid.domain.transformer

import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class SyncSchedulerTransformer<T> : SingleSchedulerTransformer<T>() {
  override fun apply(upstream: Single<T>): Single<T> {
    return upstream.subscribeOn(Schedulers.trampoline())
        .observeOn(Schedulers.trampoline())
  }
}