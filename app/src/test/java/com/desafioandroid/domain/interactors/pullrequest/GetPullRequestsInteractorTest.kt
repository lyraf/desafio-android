package com.desafioandroid.domain.interactors.pullrequest

import com.desafioandroid.data.model.PullRequest
import com.desafioandroid.domain.repository.PullRequestRepository
import com.desafioandroid.domain.transformer.SyncSchedulerTransformer
import com.desafioandroid.util.TestUtils
import com.desafioandroid.util.TestUtils.randomInt
import com.desafioandroid.util.TestUtils.randomString
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Single
import org.junit.Test

class GetPullRequestsInteractorTest {
  private val pullRequestsRepository = mockk<PullRequestRepository>()
  private val getPullRequestsInteractor =
    GetPullRequestsInteractor(pullRequestsRepository, SyncSchedulerTransformer())

  @Test
  fun `get pull requests completes`() {
    stubGetPullRequests(Single.just(TestUtils.createPullRequests()))

    val testObserver =
      getPullRequestsInteractor.execute(randomString(), randomString(), randomInt())
          .test()

    testObserver.assertComplete()
  }

  @Test fun `get pull requests returns data`() {
    val data = TestUtils.createPullRequests()
    stubGetPullRequests(Single.just(data))

    val testObserver =
      getPullRequestsInteractor.execute(randomString(), randomString(), randomInt())
          .test()

    testObserver.assertValues(data)
  }

  @Test(expected = IllegalArgumentException::class)
  fun `get pull requests empty projeto throws exception`() {
    getPullRequestsInteractor.execute("", randomString(), randomInt())
        .test()
  }

  @Test(expected = IllegalArgumentException::class)
  fun `get pull requests empty repositorio throws exception`() {
    getPullRequestsInteractor.execute(randomString(), "", randomInt())
        .test()
  }

  private fun stubGetPullRequests(single: Single<List<PullRequest>>) {
    every { pullRequestsRepository.getPullRequests(any(), any(), any()) } returns single
  }
}