package com.desafioandroid.domain.interactors.project

import com.desafioandroid.data.model.Project
import com.desafioandroid.domain.repository.ProjectRepository
import com.desafioandroid.domain.transformer.SyncSchedulerTransformer
import com.desafioandroid.util.TestUtils.createProjects
import com.desafioandroid.util.TestUtils.randomInt
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Single
import org.junit.Test

class GetProjectsRepositoryTest {
  private val projectsRepository = mockk<ProjectRepository>()
  private val getProjectsInteractor =
    GetProjectsInteractor(projectsRepository, SyncSchedulerTransformer())

  @Test
  fun `get projects completes`() {
    stubGetProjects(Single.just(createProjects()))

    val testObserver = getProjectsInteractor.execute(randomInt())
        .test()

    testObserver.assertComplete()
  }

  @Test fun `get projects returns data`() {
    val data = createProjects()
    stubGetProjects(Single.just(data))

    val testObserver = getProjectsInteractor.execute(randomInt())
        .test()

    testObserver.assertValues(data)
  }

  private fun stubGetProjects(single: Single<List<Project>>) {
    every { projectsRepository.getProjects(any()) } returns single
  }
}