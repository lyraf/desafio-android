package com.desafioandroid.remote.store

import com.desafioandroid.data.model.Project
import com.desafioandroid.data.model.PullRequest
import com.desafioandroid.remote.model.ApiResponse
import com.desafioandroid.remote.service.GitHubService
import com.desafioandroid.util.TestUtils.createApiReponseProjects
import com.desafioandroid.util.TestUtils.createProjects
import com.desafioandroid.util.TestUtils.createPullRequests
import com.desafioandroid.util.TestUtils.randomInt
import com.desafioandroid.util.TestUtils.randomString
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Single
import org.junit.Test

class RemoteStoreImplTest {
  private val service = mockk<GitHubService>()
  private val remote = RemoteStoreImpl(service)

  @Test
  fun `get projects completes`() {
    stubSearchProjects(Single.just(createApiReponseProjects(createProjects())))

    val testObserver = remote.getProjects(randomInt())
        .test()

    testObserver.assertComplete()
  }

  @Test
  fun `get projects returns data`() {
    val data = createProjects()
    stubSearchProjects(Single.just(createApiReponseProjects(data)))

    val testObserver = remote.getProjects(randomInt())
        .test()

    testObserver.assertValue(data)
  }

  @Test
  fun `get pull requests completes`() {
    stubSearchPullRequests(Single.just(createPullRequests()))

    val testObserver = remote.getPullRequests(randomString(), randomString(), randomInt())
        .test()

    testObserver.assertComplete()
  }

  @Test
  fun `get pull requests returns data`() {
    val data = createPullRequests()
    stubSearchPullRequests(Single.just(data))

    val testObserver = remote.getPullRequests(randomString(), randomString(), randomInt())
        .test()

    testObserver.assertValue(data)
  }

  private fun stubSearchProjects(single: Single<ApiResponse<Project>>) {
    every { service.searchProjects(any(), any(), any()) } returns single
  }

  private fun stubSearchPullRequests(single: Single<List<PullRequest>>) {
    every { service.searchPullRequests(any(), any(), any(), any()) } returns single
  }
}