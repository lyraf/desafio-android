package com.desafioandroid.domain.interactors.pullrequest

import com.desafioandroid.data.model.PullRequest
import com.desafioandroid.domain.repository.PullRequestRepository
import com.desafioandroid.domain.transformer.SingleSchedulerTransformer
import io.reactivex.Single

class GetPullRequestsInteractor(
  private val repository: PullRequestRepository,
  private val singleSchedulerTransformer: SingleSchedulerTransformer<List<PullRequest>>
) {
  fun execute(
    criador: String,
    projeto: String,
    page: Int
  ): Single<List<PullRequest>> {
    if (criador.isEmpty()) throw IllegalArgumentException("Criador não pode ser vazio")
    if (projeto.isEmpty()) throw IllegalArgumentException("Projeto não pode ser vazio")
    return repository.getPullRequests(criador, projeto, page)
        .compose(singleSchedulerTransformer)
  }
}