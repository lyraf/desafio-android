package com.desafioandroid.domain.interactors.project

import com.desafioandroid.data.model.Project
import com.desafioandroid.domain.repository.ProjectRepository
import com.desafioandroid.domain.transformer.SingleSchedulerTransformer
import io.reactivex.Single

class GetProjectsInteractor(
  private val projectRepository: ProjectRepository,
  private val singleSchedulerTransformer: SingleSchedulerTransformer<List<Project>>
) {
  fun execute(params: Int): Single<List<Project>> {
    return projectRepository.getProjects(params)
        .compose(singleSchedulerTransformer)
  }
}