package com.desafioandroid.domain.transformer

import io.reactivex.SingleTransformer

abstract class SingleSchedulerTransformer<T> : SingleTransformer<T, T>