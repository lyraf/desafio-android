package com.desafioandroid.domain.repository

import com.desafioandroid.data.model.PullRequest
import io.reactivex.Single

interface PullRequestRepository {
  fun getPullRequests(
    criador: String,
    projeto: String,
    page: Int
  ): Single<List<PullRequest>>
}