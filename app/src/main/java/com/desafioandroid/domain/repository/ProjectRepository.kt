package com.desafioandroid.domain.repository

import com.desafioandroid.data.model.Project
import io.reactivex.Single

interface ProjectRepository {
  fun getProjects(page: Int): Single<List<Project>>
}