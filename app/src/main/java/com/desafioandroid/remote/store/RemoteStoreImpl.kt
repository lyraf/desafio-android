package com.desafioandroid.remote.store

import com.desafioandroid.data.model.PullRequest
import com.desafioandroid.data.model.Project
import com.desafioandroid.data.store.RemoteStore
import com.desafioandroid.remote.service.GitHubService
import io.reactivex.Single

class RemoteStoreImpl(private val service: GitHubService) : RemoteStore {
  override fun getProjects(page: Int): Single<List<Project>> {
    return service.searchProjects("language:kotlin", "stars", page)
        .map { it.items }
  }

  override fun getPullRequests(
    criador: String,
    projeto: String,
    page: Int
  ): Single<List<PullRequest>> {
    return service.searchPullRequests(criador, projeto, "all", page)
  }
}