package com.desafioandroid.remote.model

data class ApiResponse<T>(val items: List<T>)