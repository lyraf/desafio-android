package com.desafioandroid.remote.interceptors

import com.desafioandroid.remote.errors.InternetUnavailableException
import okhttp3.Interceptor
import okhttp3.Response

abstract class NetworkConnectionInterceptor : Interceptor {
  abstract fun isInternetAvailable(): Boolean

  override fun intercept(chain: Interceptor.Chain): Response {
    val request = chain.request()
    if (!isInternetAvailable()) {
      throw InternetUnavailableException()
    }
    return chain.proceed(request)
  }
}