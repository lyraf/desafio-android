package com.desafioandroid.remote.interceptors

import com.desafioandroid.remote.errors.ForbiddenException
import okhttp3.Interceptor
import okhttp3.Response

class ServerErrorInterceptor : Interceptor {
  override fun intercept(chain: Interceptor.Chain): Response {
    val request = chain.request()
    val response = chain.proceed(request)

    when (response.code()) {
      403 -> throw ForbiddenException()
      else -> return response
    }
  }
}