package com.desafioandroid.remote.service

import com.desafioandroid.remote.model.ApiResponse
import com.desafioandroid.data.model.Project
import com.desafioandroid.data.model.PullRequest
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GitHubService {
  @GET("search/repositories")
  fun searchProjects(
    @Query("q") query: String,
    @Query("sort") sort: String,
    @Query("page") page: Int
  ): Single<ApiResponse<Project>>

  @GET("repos/{criador}/{projeto}/pulls")
  fun searchPullRequests(
    @Path("criador") criador: String,
    @Path("projeto") projeto: String,
    @Query("state") state: String,
    @Query("page") page: Int
  ): Single<List<PullRequest>>
}