package com.desafioandroid.remote.service

import com.desafioandroid.device.connection.NetworkConnectionChecker
import com.desafioandroid.remote.interceptors.NetworkConnectionInterceptor
import com.desafioandroid.remote.interceptors.ServerErrorInterceptor
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BASIC
import okhttp3.logging.HttpLoggingInterceptor.Level.NONE
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.io.File

object GitHubServiceFactory {
  var apiUrl = "https://api.github.com/"

  fun createService(
      cacheDir: File,
      networkConnectionChecker: NetworkConnectionChecker,
      isDebug: Boolean
  ): GitHubService {
    val cache = createCache(cacheDir)
    val loggingInterceptor = createLoggingInterceptor(isDebug)
    val serverErrorInterceptor = createServerErrorInterceptor()
    val connectionInterceptor = createNetworkConnectionInterceptor(networkConnectionChecker)
    val client = createOkhttpClient(
        cache, loggingInterceptor, serverErrorInterceptor, connectionInterceptor
    )
    return createApi(client)
  }

  private fun createCache(cacheDir: File): Cache {
    val cacheSize = 10 * 1024 * 1024
    return Cache(cacheDir, cacheSize.toLong())
  }

  private fun createLoggingInterceptor(isDebug: Boolean): HttpLoggingInterceptor {
    val interceptor = HttpLoggingInterceptor()
    if (isDebug) {
      interceptor.level = BASIC
    } else {
      interceptor.level = NONE
    }
    return interceptor
  }

  private fun createServerErrorInterceptor(): ServerErrorInterceptor {
    return ServerErrorInterceptor()
  }

  private fun createNetworkConnectionInterceptor(
      networkConnectionChecker: NetworkConnectionChecker
  ): NetworkConnectionInterceptor {
    return object : NetworkConnectionInterceptor() {
      override fun isInternetAvailable(): Boolean {
        return networkConnectionChecker.isConnectionAvaiable()
      }
    }
  }

  private fun createOkhttpClient(
      cache: Cache,
      loggingInterceptor: HttpLoggingInterceptor,
      serverErrorInterceptor: ServerErrorInterceptor,
      connectionInterceptor: NetworkConnectionInterceptor
  ): OkHttpClient {
    return OkHttpClient.Builder()
        .cache(cache)
        .addInterceptor(loggingInterceptor)
        .addInterceptor(serverErrorInterceptor)
        .addInterceptor(connectionInterceptor)
        .build()
  }

  private fun createApi(client: OkHttpClient): GitHubService {
    return Retrofit.Builder()
        .baseUrl(apiUrl)
        .client(client)
        .addConverterFactory(MoshiConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(GitHubService::class.java)
  }
}