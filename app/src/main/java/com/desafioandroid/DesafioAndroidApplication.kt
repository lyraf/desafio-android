package com.desafioandroid

import android.app.Application
import com.desafioandroid.ui.browseprojects.browseProjectsModule
import com.desafioandroid.ui.browsepullrequests.browsePullRequestsModule
import com.desafioandroid.ui.di.androidModule
import com.desafioandroid.ui.di.deviceModule
import com.desafioandroid.ui.di.remoteModule
import org.koin.android.ext.android.startKoin
import timber.log.Timber
import timber.log.Timber.DebugTree

class DesafioAndroidApplication : Application() {
  override fun onCreate() {
    super.onCreate()

    startKoin(
        this,
        listOf(
            androidModule,
            deviceModule,
            remoteModule,
            browseProjectsModule,
            browsePullRequestsModule
        )
    )

    if (BuildConfig.DEBUG) Timber.plant(DebugTree())
  }
}