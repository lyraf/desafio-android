package com.desafioandroid.presentation.viewmodel

import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel : ViewModel(), LifecycleObserver {
  protected val disposable = CompositeDisposable()

  override fun onCleared() {
    disposable.clear()
  }
}