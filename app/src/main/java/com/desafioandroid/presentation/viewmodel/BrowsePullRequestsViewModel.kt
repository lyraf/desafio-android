package com.desafioandroid.presentation.viewmodel

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.OnLifecycleEvent
import com.desafioandroid.data.model.Project
import com.desafioandroid.data.model.PullRequest
import com.desafioandroid.domain.interactors.pullrequest.GetPullRequestsInteractor
import com.desafioandroid.presentation.state.BrowseProjectsState
import com.desafioandroid.presentation.state.BrowsePullRequestsState
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.ReplaySubject
import timber.log.Timber

class BrowsePullRequestsViewModel(
  private val getPullRequestsInteractor: GetPullRequestsInteractor
) : BaseViewModel() {
  private val state = BehaviorSubject.create<BrowsePullRequestsState>()
  var nomeCriador = ""
  var nomeRepositorio = ""
  private var currentPage = 1
  private val pullRequests = mutableListOf<PullRequest>()

  fun getState(): BehaviorSubject<BrowsePullRequestsState> {
    return state
  }

  fun fetchPullRequests(
    page: Int
  ) {
    Timber.i("Fetch Pull Requests Page: %s", page.toString())

    currentPage = page

    getPullRequestsInteractor.execute(nomeCriador, nomeRepositorio, page)
        .subscribeBy(
            onSuccess = {
              pullRequests.addAll(it)
              state.onNext(BrowsePullRequestsState.Data(it))
            },
            onError = {
              state.onNext(BrowsePullRequestsState.Error(it))
            }
        )
        .addTo(disposable)
  }

  fun retryFetch() {
    fetchPullRequests(currentPage)
  }

  @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
  fun fetchPullRequestsIfNeeded() {
    if (!pullRequests.isEmpty()) {
      state.onNext(BrowsePullRequestsState.Data(pullRequests))
    } else if (!state.hasValue()) {
      fetchPullRequests(1)
    }
  }
}