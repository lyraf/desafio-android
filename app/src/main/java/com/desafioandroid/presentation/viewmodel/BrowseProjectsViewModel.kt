package com.desafioandroid.presentation.viewmodel

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.OnLifecycleEvent
import android.util.Log
import com.desafioandroid.data.model.Project
import com.desafioandroid.domain.interactors.project.GetProjectsInteractor
import com.desafioandroid.presentation.state.BrowseProjectsState
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.ReplaySubject
import timber.log.Timber

class BrowseProjectsViewModel(
  private val getProjectsInteractor: GetProjectsInteractor
) : BaseViewModel() {
  private val state = BehaviorSubject.create<BrowseProjectsState>()
  private val projects = mutableListOf<Project>()
  private var currentPage = 1

  fun getState(): BehaviorSubject<BrowseProjectsState> {
    return state
  }

  fun fetchProjects(page: Int) {
    Timber.i("Fetch Project Page: %s", page.toString())
    currentPage = page

    getProjectsInteractor.execute(page)
        .subscribeBy(
            onSuccess = {
              if (!projects.containsAll(it)) projects.addAll(it)

              state.onNext(BrowseProjectsState.Data(it))
            },
            onError = {
              state.onNext(BrowseProjectsState.Error(it))
            }
        )
        .addTo(disposable)
  }

  fun retryFetch() {
    fetchProjects(currentPage)
  }

  @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
  fun fetchProjectsIfNeeded() {
    if (!projects.isEmpty()) {
      state.onNext(BrowseProjectsState.Data(projects))
    } else if (!state.hasValue()) {
      fetchProjects(1)
    }
  }
}