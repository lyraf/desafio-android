package com.desafioandroid.presentation.state

import com.desafioandroid.data.model.Project

sealed class BrowseProjectsState {
  data class Error(val error: Throwable) : BrowseProjectsState()
  data class Data(val data: List<Project>) : BrowseProjectsState()
}