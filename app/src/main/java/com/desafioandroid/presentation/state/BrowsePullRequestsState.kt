package com.desafioandroid.presentation.state

import com.desafioandroid.data.model.PullRequest

sealed class BrowsePullRequestsState {
  data class Error(val error: Throwable) : BrowsePullRequestsState()
  data class Data(val data: List<PullRequest>) : BrowsePullRequestsState()
}