package com.desafioandroid.ui.browseprojects

import com.desafioandroid.data.model.Project
import com.desafioandroid.data.repository.ProjectRepositoryImpl
import com.desafioandroid.data.store.RemoteStore
import com.desafioandroid.domain.interactors.project.GetProjectsInteractor
import com.desafioandroid.domain.repository.ProjectRepository
import com.desafioandroid.domain.transformer.SingleSchedulerTransformer
import com.desafioandroid.presentation.viewmodel.BrowseProjectsViewModel
import com.desafioandroid.remote.store.RemoteStoreImpl
import com.desafioandroid.ui.transformer.AsyncSchedulerTransformer
import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.module.applicationContext

val browseProjectsModule = applicationContext {
  bean { RemoteStoreImpl(get()) as RemoteStore }
  bean { ProjectRepositoryImpl(get()) as ProjectRepository }
  bean { AsyncSchedulerTransformer<List<Project>>() as SingleSchedulerTransformer<List<Project>> }
  bean { GetProjectsInteractor(get(), get()) }
  viewModel { BrowseProjectsViewModel(get()) }
  factory { BrowseProjectsAdapter() }
}