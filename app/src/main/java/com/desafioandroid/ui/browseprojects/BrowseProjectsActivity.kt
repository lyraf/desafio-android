package com.desafioandroid.ui.browseprojects

import android.content.Intent
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.View.GONE
import android.view.View.VISIBLE
import com.desafioandroid.R
import com.desafioandroid.data.model.Project
import com.desafioandroid.extension.action
import com.desafioandroid.extension.snack
import com.desafioandroid.presentation.state.BrowseProjectsState
import com.desafioandroid.presentation.viewmodel.BrowseProjectsViewModel
import com.desafioandroid.remote.errors.ForbiddenException
import com.desafioandroid.remote.errors.InternetUnavailableException
import com.desafioandroid.ui.base.BaseActivity
import com.desafioandroid.ui.browsepullrequests.BrowsePullRequestsActivity
import com.desafioandroid.ui.browsepullrequests.BrowsePullRequestsActivity.Companion.EXTRA_NOME_CRIADOR
import com.desafioandroid.ui.browsepullrequests.BrowsePullRequestsActivity.Companion.EXTRA_NOME_REPOSITORIO
import com.desafioandroid.ui.common.EndlessScrollListener
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.activity_browse_projects.projectsParent
import kotlinx.android.synthetic.main.layout_list_empty.listEmpty
import kotlinx.android.synthetic.main.layout_list_refresh.list
import kotlinx.android.synthetic.main.layout_list_refresh.listLoading
import kotlinx.android.synthetic.main.layout_toolbar.toolbar
import org.koin.android.architecture.ext.viewModel
import org.koin.android.ext.android.inject
import timber.log.Timber

class BrowseProjectsActivity : BaseActivity() {
  private val browseProjectsViewModel: BrowseProjectsViewModel by viewModel()
  private val adapter: BrowseProjectsAdapter by inject()
  val layoutManager: LinearLayoutManager by inject()
  private val disposable = CompositeDisposable()

  override fun getLayout(): Int {
    return R.layout.activity_browse_projects
  }

  override fun setupUi() {
    setSupportActionBar(toolbar)

    list.layoutManager = layoutManager
    list.addItemDecoration(DividerItemDecoration(this, layoutManager.orientation))
    list.adapter = adapter

    list.addOnScrollListener(object : EndlessScrollListener(layoutManager) {
      override fun onLoadMore(page: Int) {
        browseProjectsViewModel.fetchProjects(page)
      }
    })

    listLoading.setColorSchemeResources(R.color.blue_a200)
    listLoading.setOnRefreshListener { listLoading.isRefreshing = false }
    listLoading.isRefreshing = true

    listEmpty.text = resources.getString(R.string.repositories_empty)
  }

  override fun setupObservers() {
    lifecycle.addObserver(browseProjectsViewModel)

    browseProjectsViewModel.getState()
        .subscribeBy { state ->
          when (state) {
            is BrowseProjectsState.Error -> showErrorState(state.error)
            is BrowseProjectsState.Data -> showDataState(state.data)
          }
        }
        .addTo(disposable)

    adapter.getProjectClicks()
        .subscribeBy {
          Timber.d("Repository clicked: %s", it.toString())
          val intent = Intent(
              this,
              BrowsePullRequestsActivity::class.java
          ).apply {
            putExtra(EXTRA_NOME_CRIADOR, it.user.login)
            putExtra(EXTRA_NOME_REPOSITORIO, it.name)
          }
          startActivity(intent)
        }
        .addTo(disposable)
  }

  private fun showErrorState(error: Throwable) {
    Timber.d("Error: %s", error.localizedMessage)
    listLoading.isRefreshing = false

    when (error) {
      is ForbiddenException -> showMessage(resources.getString(R.string.error_api_limit))
      is InternetUnavailableException -> showMessage(resources.getString(R.string.error_network))
    }
  }

  private fun showDataState(data: List<Project>) {
    Timber.d("Repositórios: %s", data.size)
    listLoading.isRefreshing = false
    listEmpty.visibility = GONE

    list.visibility = VISIBLE
    adapter.updateDataSet(data)
  }

  private fun showMessage(message: String) {
    projectsParent.snack(message, Snackbar.LENGTH_INDEFINITE) {
      action(
          resources.getString(R.string.action_retry),
          ContextCompat.getColor(context, R.color.blue_a200),
          listener = { browseProjectsViewModel.retryFetch() }
      )
    }
  }
}
