package com.desafioandroid.ui.browseprojects

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.desafioandroid.R
import com.desafioandroid.data.model.Project
import com.desafioandroid.extension.loadUrl
import com.desafioandroid.ui.browseprojects.BrowseProjectsAdapter.ViewHolder
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_project.view.avatar
import kotlinx.android.synthetic.main.item_project.view.descricao
import kotlinx.android.synthetic.main.item_project.view.estrelas
import kotlinx.android.synthetic.main.item_project.view.forks
import kotlinx.android.synthetic.main.item_project.view.login
import kotlinx.android.synthetic.main.item_project.view.nome

class BrowseProjectsAdapter : Adapter<ViewHolder>() {
  private var dataset = mutableListOf<Project>()
  private val projectClick = PublishSubject.create<Project>()

  fun updateDataSet(projects: List<Project>) {
    if (!dataset.containsAll(projects)) {
      val positionStart = dataset.size + 1
      dataset.addAll(projects)
      notifyItemRangeInserted(positionStart, projects.size)
    }
  }

  fun getProjectClicks(): PublishSubject<Project> {
    return projectClick
  }

  override fun onCreateViewHolder(
      parent: ViewGroup,
      viewType: Int
  ): ViewHolder {
    val view = LayoutInflater.from(parent.context)
        .inflate(R.layout.item_project, parent, false)

    return ViewHolder(view)
  }

  override fun getItemCount(): Int {
    return dataset.size
  }

  override fun onBindViewHolder(
      holder: ViewHolder,
      position: Int
  ) {
    holder.bind(dataset[position], projectClick)
  }

  class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(
        project: Project,
        projectClick: PublishSubject<Project>
    ) {
      itemView.nome.text = project.name
      if (!project.description.isNullOrEmpty()) itemView.descricao.text = project.description
      itemView.estrelas.text = project.starCount.toString()
      itemView.forks.text = project.forkCount.toString()
      itemView.login.text = project.user.login
      itemView.avatar.loadUrl(
          project.user.avatar,
          R.drawable.ic_default_avatar,
          R.drawable.ic_default_avatar
      )

      itemView.setOnClickListener {
        projectClick.onNext(project)
      }
    }
  }
}