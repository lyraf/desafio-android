package com.desafioandroid.ui.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.layout_toolbar.toolbar

abstract class BaseActivity : AppCompatActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(getLayout())
    setSupportActionBar(toolbar)

    setupUi()
    setupObservers()
  }

  abstract fun getLayout(): Int
  abstract fun setupUi()
  abstract fun setupObservers()
}