package com.desafioandroid.ui.transformer

import com.desafioandroid.domain.transformer.SingleSchedulerTransformer
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AsyncSchedulerTransformer<T> : SingleSchedulerTransformer<T>() {
  override fun apply(upstream: Single<T>): Single<T> {
    return upstream.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
  }
}