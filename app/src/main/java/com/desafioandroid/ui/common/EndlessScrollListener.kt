package com.desafioandroid.ui.common

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView

abstract class EndlessScrollListener(private val layoutManager: LinearLayoutManager) : RecyclerView.OnScrollListener() {
  private var visibleThreshold = 5
  private var previousTotal = 0
  private var loading = true
  private var currentPage = 1

  override fun onScrolled(
    recyclerView: RecyclerView?,
    dx: Int,
    dy: Int
  ) {
    super.onScrolled(recyclerView, dx, dy)

    val visibleItemCount = layoutManager.childCount
    val totalItemCount = layoutManager.itemCount
    val firstVisibleItem = layoutManager.findFirstVisibleItemPosition()

    if (loading) {
      if (totalItemCount > previousTotal) {
        loading = false
        previousTotal = totalItemCount
      }
    }

    if (!loading && visibleItemCount <= firstVisibleItem + visibleThreshold) {
      loading = true
      currentPage++
      onLoadMore(currentPage)
    }
  }

  abstract fun onLoadMore(page: Int)
}