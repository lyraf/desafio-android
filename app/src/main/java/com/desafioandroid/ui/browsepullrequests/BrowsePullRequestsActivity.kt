package com.desafioandroid.ui.browsepullrequests

import android.net.Uri
import android.os.Bundle
import android.support.customtabs.CustomTabsIntent
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.desafioandroid.R
import com.desafioandroid.R.string
import com.desafioandroid.data.model.PullRequest
import com.desafioandroid.extension.action
import com.desafioandroid.extension.snack
import com.desafioandroid.presentation.state.BrowsePullRequestsState
import com.desafioandroid.presentation.viewmodel.BrowsePullRequestsViewModel
import com.desafioandroid.remote.errors.ForbiddenException
import com.desafioandroid.remote.errors.InternetUnavailableException
import com.desafioandroid.ui.base.BaseActivity
import com.desafioandroid.ui.common.EndlessScrollListener
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.activity_browse_projects.projectsParent
import kotlinx.android.synthetic.main.activity_browse_pull_requests.pullRequestsParent
import kotlinx.android.synthetic.main.layout_list_empty.listEmpty
import kotlinx.android.synthetic.main.layout_list_refresh.list
import kotlinx.android.synthetic.main.layout_list_refresh.listLoading
import kotlinx.android.synthetic.main.layout_toolbar.toolbar
import org.koin.android.architecture.ext.viewModel
import org.koin.android.ext.android.inject
import timber.log.Timber
import java.io.IOException

class BrowsePullRequestsActivity : BaseActivity() {
  private val browsePullRequestsViewModel: BrowsePullRequestsViewModel by viewModel()
  private val adapter: BrowsePullRequestsAdapter by inject()
  val layoutManager: LinearLayoutManager by inject()
  private val disposable = CompositeDisposable()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    browsePullRequestsViewModel.nomeCriador = intent.getStringExtra(EXTRA_NOME_CRIADOR)

    val nomeRepositorio = intent.getStringExtra(EXTRA_NOME_REPOSITORIO)
    browsePullRequestsViewModel.nomeRepositorio = nomeRepositorio
    toolbar.title = nomeRepositorio

  }

  override fun getLayout(): Int {
    return R.layout.activity_browse_pull_requests
  }

  override fun setupUi() {
    setSupportActionBar(toolbar)
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    showOpenCloseTotal()

    list.layoutManager = layoutManager
    list.addItemDecoration(DividerItemDecoration(this, layoutManager.orientation))
    list.adapter = adapter

    list.addOnScrollListener(object : EndlessScrollListener(layoutManager) {
      override fun onLoadMore(page: Int) {
        browsePullRequestsViewModel.fetchPullRequests(page)
      }
    })

    listLoading.setColorSchemeResources(R.color.blue_a200)
    listLoading.setOnRefreshListener { listLoading.isRefreshing = false }
    listLoading.isRefreshing = true

    listEmpty.text = resources.getString(R.string.pull_requests_empty)
  }

  override fun setupObservers() {
    lifecycle.addObserver(browsePullRequestsViewModel)

    browsePullRequestsViewModel.getState()
        .subscribeBy { state ->
          when (state) {
            is BrowsePullRequestsState.Error -> showErrorState(state.error)
            is BrowsePullRequestsState.Data -> showDataState(state.data)
          }
        }
        .addTo(disposable)

    adapter.getPullRequestClicks()
        .subscribeBy {
          Timber.d("Pull Request clicked: %s", it.htmlUrl)
          val builder = CustomTabsIntent.Builder()
          builder.setToolbarColor(ContextCompat.getColor(this, R.color.deep_purple_700))
          val customTabsIntent = builder.build()
          customTabsIntent.launchUrl(this, Uri.parse(it.htmlUrl))
        }
        .addTo(disposable)
  }

  private fun showErrorState(error: Throwable) {
    Timber.d("Error: %s", error.localizedMessage)
    listLoading.isRefreshing = false

    when (error) {
      is ForbiddenException -> showMessage(resources.getString(R.string.error_api_limit))
      is InternetUnavailableException -> showMessage(resources.getString(R.string.error_network))
    }
  }

  private fun showDataState(data: List<PullRequest>) {
    Timber.d("Pull Requests: %s", data.size)
    listLoading.isRefreshing = false
    listEmpty.visibility = View.GONE

    list.visibility = View.VISIBLE
    adapter.updateDataSet(data)

    showOpenCloseTotal()
  }

  private fun showOpenCloseTotal() {
    val countOpen = adapter.countOpen()
    val countClosed = adapter.countClosed()
    toolbar.subtitle = getString(string.pull_request_total_status, countOpen, countClosed)
  }

  private fun showMessage(message: String) {
    pullRequestsParent.snack(message, Snackbar.LENGTH_INDEFINITE) {
      action(
          resources.getString(R.string.action_retry),
          ContextCompat.getColor(context, R.color.blue_a200),
          listener = { browsePullRequestsViewModel.retryFetch() }
      )
    }
  }

  companion object {
    const val EXTRA_NOME_CRIADOR = "extraNomeCriador"
    const val EXTRA_NOME_REPOSITORIO = "extraNomeRepositorio"
  }
}
