package com.desafioandroid.ui.browsepullrequests

import com.desafioandroid.data.model.PullRequest
import com.desafioandroid.data.repository.PullRequestRepositoryImpl
import com.desafioandroid.data.store.RemoteStore
import com.desafioandroid.domain.interactors.pullrequest.GetPullRequestsInteractor
import com.desafioandroid.domain.repository.PullRequestRepository
import com.desafioandroid.domain.transformer.SingleSchedulerTransformer
import com.desafioandroid.presentation.viewmodel.BrowsePullRequestsViewModel
import com.desafioandroid.remote.store.RemoteStoreImpl
import com.desafioandroid.ui.browseprojects.BrowseProjectsAdapter
import com.desafioandroid.ui.transformer.AsyncSchedulerTransformer
import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.module.applicationContext

val browsePullRequestsModule = applicationContext {
  bean { RemoteStoreImpl(get()) as RemoteStore }
  bean { PullRequestRepositoryImpl(get()) as PullRequestRepository }
  bean { AsyncSchedulerTransformer<List<PullRequest>>() as SingleSchedulerTransformer<List<PullRequest>> }
  bean { GetPullRequestsInteractor(get(), get()) }
  viewModel { BrowsePullRequestsViewModel(get()) }
  factory { BrowsePullRequestsAdapter() }
}