package com.desafioandroid.ui.browsepullrequests

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.support.v7.widget.RecyclerView.VISIBLE
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import com.desafioandroid.R
import com.desafioandroid.data.model.PullRequest
import com.desafioandroid.extension.formatTimestamp
import com.desafioandroid.extension.loadUrl
import com.desafioandroid.ui.browsepullrequests.BrowsePullRequestsAdapter.ViewHolder
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_pull_request.view.avatar
import kotlinx.android.synthetic.main.item_pull_request.view.data
import kotlinx.android.synthetic.main.item_pull_request.view.descricao
import kotlinx.android.synthetic.main.item_pull_request.view.login
import kotlinx.android.synthetic.main.item_pull_request.view.statusClosed
import kotlinx.android.synthetic.main.item_pull_request.view.statusOpen
import kotlinx.android.synthetic.main.item_pull_request.view.statusOther
import kotlinx.android.synthetic.main.item_pull_request.view.titulo

class BrowsePullRequestsAdapter : Adapter<ViewHolder>() {
  private var dataset = mutableListOf<PullRequest>()
  private val pullRequestClick = PublishSubject.create<PullRequest>()

  fun updateDataSet(pullRequests: List<PullRequest>) {
    if (!dataset.containsAll(pullRequests)) {
      val positionStart = dataset.size + 1
      dataset.addAll(pullRequests)
      notifyItemRangeInserted(positionStart, pullRequests.size)
    }
  }

  fun countOpen(): Int {
    return dataset.count { it.state == "open" }
  }

  fun countClosed(): Int {
    return dataset.count { it.state == "closed" }
  }

  fun getPullRequestClicks(): PublishSubject<PullRequest> {
    return pullRequestClick
  }

  override fun onCreateViewHolder(
      parent: ViewGroup,
      viewType: Int
  ): ViewHolder {
    val view = LayoutInflater.from(parent.context)
        .inflate(R.layout.item_pull_request, parent, false)

    return ViewHolder(view)
  }

  override fun getItemCount(): Int {
    return dataset.size
  }

  override fun onBindViewHolder(
      holder: ViewHolder,
      position: Int
  ) {
    holder.bind(dataset[position], pullRequestClick)
  }

  class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(
        pullRequest: PullRequest,
        pullRequestClick: PublishSubject<PullRequest>
    ) {
      itemView.titulo.text = pullRequest.user.login
      if (!pullRequest.body.isEmpty()) itemView.descricao.text = pullRequest.body
      itemView.avatar.loadUrl(
          pullRequest.user.avatar,
          R.drawable.ic_default_avatar,
          R.drawable.ic_default_avatar
      )
      itemView.login.text = pullRequest.user.login
      itemView.data.text = pullRequest.date.formatTimestamp()

      when {
        pullRequest.state == "open" -> {
          itemView.statusOpen.visibility = VISIBLE
          itemView.statusClosed.visibility = GONE
          itemView.statusOther.visibility = GONE
        }
        pullRequest.state == "closed" -> {
          itemView.statusClosed.visibility = VISIBLE
          itemView.statusOpen.visibility = GONE
          itemView.statusOther.visibility = GONE
        }
        else -> {
          itemView.statusOther.visibility = VISIBLE
          itemView.statusClosed.visibility = GONE
          itemView.statusOpen.visibility = GONE
        }
      }

      itemView.setOnClickListener {
        pullRequestClick.onNext(pullRequest)
      }
    }
  }
}