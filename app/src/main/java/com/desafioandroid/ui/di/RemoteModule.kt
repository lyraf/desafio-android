package com.desafioandroid.ui.di

import com.desafioandroid.BuildConfig
import com.desafioandroid.remote.service.GitHubServiceFactory
import org.koin.dsl.module.applicationContext

val remoteModule = applicationContext {
  bean { GitHubServiceFactory.createService(get(), get(), BuildConfig.DEBUG) }
}
