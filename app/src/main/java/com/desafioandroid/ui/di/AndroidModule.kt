package com.desafioandroid.ui.di

import android.content.Context
import android.net.ConnectivityManager
import android.support.v7.widget.LinearLayoutManager
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.applicationContext

val androidModule = applicationContext {
  bean { androidApplication().cacheDir }
  bean {
    androidApplication().getSystemService(
        Context.CONNECTIVITY_SERVICE
    ) as ConnectivityManager
  }
  factory { LinearLayoutManager(androidApplication()) }
}