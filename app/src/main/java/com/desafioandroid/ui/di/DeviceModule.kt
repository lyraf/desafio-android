package com.desafioandroid.ui.di

import com.desafioandroid.device.connection.NetworkConnectionChecker
import org.koin.dsl.module.applicationContext

val deviceModule = applicationContext {
  bean { NetworkConnectionChecker(get()) }
}