package com.desafioandroid.data.store

import com.desafioandroid.data.model.PullRequest
import com.desafioandroid.data.model.Project
import io.reactivex.Single

interface RemoteStore {
  fun getProjects(page: Int): Single<List<Project>>

  fun getPullRequests(
    criador: String,
    projeto: String,
    page: Int
  ): Single<List<PullRequest>>
}