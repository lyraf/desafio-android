package com.desafioandroid.data.repository

import com.desafioandroid.data.model.PullRequest
import com.desafioandroid.data.store.RemoteStore
import com.desafioandroid.domain.repository.PullRequestRepository
import io.reactivex.Single

class PullRequestRepositoryImpl(private val remote: RemoteStore) : PullRequestRepository {
  override fun getPullRequests(
    criador: String,
    projeto: String,
    page: Int
  ): Single<List<PullRequest>> {
    return remote.getPullRequests(criador, projeto, page)
  }
}