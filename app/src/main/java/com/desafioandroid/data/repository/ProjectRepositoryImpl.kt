package com.desafioandroid.data.repository

import com.desafioandroid.data.model.Project
import com.desafioandroid.data.store.RemoteStore
import com.desafioandroid.domain.repository.ProjectRepository
import io.reactivex.Single

class ProjectRepositoryImpl(private val remote: RemoteStore) : ProjectRepository {
  override fun getProjects(page: Int): Single<List<Project>> {
    return remote.getProjects(page)
  }
}