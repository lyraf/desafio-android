package com.desafioandroid.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Project(
  val name: String,
  val description: String?,
  @Json(name = "stargazers_count") val starCount: Int,
  @Json(name = "forks_count") val forkCount: Int,
  @Json(name = "owner") val user: User
)