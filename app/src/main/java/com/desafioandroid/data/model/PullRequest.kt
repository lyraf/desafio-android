package com.desafioandroid.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PullRequest(
  val title: String,
  val body: String,
  val state: String,
  @Json(name = "html_url") val htmlUrl: String,
  @Json(name = "created_at") val date: String,
  @Json(name = "user") val user: User
)