package com.desafioandroid.extension

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Locale

private val timestampFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault())
private val dateFormatter = SimpleDateFormat("MM/dd/yyyy", Locale.getDefault())

fun String.formatTimestamp(): String {
  return try {
    val date = timestampFormatter.parse(this)
    dateFormatter.format(date)
  } catch (e: ParseException) {
    ""
  }
}
