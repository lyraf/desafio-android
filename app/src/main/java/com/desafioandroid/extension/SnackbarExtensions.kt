package com.desafioandroid.extension

import android.support.design.widget.Snackbar
import android.view.View

fun Snackbar.action(
  action: String,
  actionColor: Int,
  listener: (View) -> Unit
) {
  setAction(action, listener)
  setActionTextColor(actionColor)
}