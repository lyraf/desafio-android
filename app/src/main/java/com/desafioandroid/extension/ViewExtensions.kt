package com.desafioandroid.extension

import android.support.design.widget.Snackbar
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

inline fun View.snack(
  message: String,
  length: Int,
  f: Snackbar.() -> Unit
) {
  val snack = Snackbar.make(this, message, length)
  snack.f()
  snack.show()
}

fun ImageView.loadUrl(
  url: String,
  placeholder: Int,
  error: Int
) {
  Glide.with(context)
      .load(url)
      .apply(
          RequestOptions()
              .placeholder(placeholder)
              .error(error)
              .circleCrop()
      )
      .into(this)
}