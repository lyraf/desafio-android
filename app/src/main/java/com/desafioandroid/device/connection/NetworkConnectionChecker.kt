package com.desafioandroid.device.connection

import android.net.ConnectivityManager

class NetworkConnectionChecker(private val connectivityManager: ConnectivityManager) {
  fun isConnectionAvaiable(): Boolean {
    return connectivityManager.activeNetworkInfo != null && connectivityManager.activeNetworkInfo.isConnected
  }
}